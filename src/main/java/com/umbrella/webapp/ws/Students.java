package com.umbrella.webapp.ws;

import com.umbrella.webapp.biz.StudentManager;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

/**
 *
 * @author d.traverso
 */
@Path("students")
@RequestScoped
public class Students {
    
    public Students(){}
    
    @Inject
    StudentManager studentManager;
    
    @GET
    public String getDefaultStudent(){
        return studentManager.getDefaultStudent().toString();
    }    
}