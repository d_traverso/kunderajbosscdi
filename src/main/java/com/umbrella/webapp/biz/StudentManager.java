package com.umbrella.webapp.biz;

import com.umbrella.webapp.entity.Student;
import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

/**
 *
 * @author d.traverso
 */
public class StudentManager {
    
    @PersistenceUnit(unitName = "cassandra_pu")
    private EntityManagerFactory entityManagerFactory;
    private EntityManager em;

    public StudentManager() {
    }

    @PostConstruct
    public void init() {
        em = entityManagerFactory.createEntityManager();
    }

    public Student getDefaultStudent() {
        Student defaultStudent = new Student();
        defaultStudent.setName("Default Student");
        return defaultStudent;
    }

    public Student getStudent(Long id) {
        return em.find(Student.class, id);
    }

    public void createStudent(Student student) {
        em.persist(student);
    }    
}